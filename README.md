## Project for experiments with environment

>Python tests launched in Selenoid, GitLab CI with Allure report

>Autotests launching in separated Selenoid Chrome browsers based on Docker image in Jenkins environment.

> Allure create reports for running tests

Selenoid useful for launching browsers independently, full information you could find in [documentation](https://aerokube.com/selenoid/latest/)

Next goal start tests in 2 threads, today in only 1.