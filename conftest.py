import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


@pytest.fixture(scope="function")
def browser():
    # browser = webdriver.Chrome()  # for local launch

    chrome_options = webdriver.ChromeOptions()
    chrome_options = Options()
    chrome_options.add_argument("--no-sandbox")
    # chrome_options.add_argument("--headless")
    browser = webdriver.Remote(command_executor='http://selenoid__chrome:4444',
                               options=chrome_options,
                               desired_capabilities=DesiredCapabilities.CHROME)


    browser.maximize_window()

    yield browser

    browser.quit()
