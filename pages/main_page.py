import allure

from .base_page import BasePage
from .data import TestData
from .locators import NamedElement


class MainPage(BasePage):
    @allure.step("Заполнить поле {locator_name} текстом {text}")
    def send_text_to_field(self, text, search_method, locator, locator_name):
        self.browser.find_element(search_method, locator).clear()
        self.browser.find_element(search_method, locator).send_keys(text)

    def fill_value_positive(self):
        self.send_text_to_field(TestData.POS_AGE, NamedElement.AGE.search_method,
                                NamedElement.AGE.locator, NamedElement.AGE.locator_name)
        self.send_text_to_field(TestData.POS_HEIGHT, NamedElement.HEIGHT.search_method,
                                NamedElement.HEIGHT.locator, NamedElement.HEIGHT.locator_name)
        self.send_text_to_field(TestData.POS_WEIGHT, NamedElement.WEIGHT.search_method, NamedElement.WEIGHT.locator,
                                NamedElement.WEIGHT.locator_name)

        self.click_element(NamedElement.GENDER.search_method, NamedElement.GENDER.locator, NamedElement.GENDER.locator_name)
        self.click_element(NamedElement.TYPE.search_method, NamedElement.TYPE.locator, NamedElement.TYPE.locator_name)
        self.click_element(NamedElement.SUBMIT.search_method, NamedElement.SUBMIT.locator, NamedElement.SUBMIT.locator_name)

    @allure.step("Проверить что калории равны {volume}")
    def check_calories(self, volume):
        number_calories = self.browser.find_element(NamedElement.CALORIES_NUMBER.search_method,
                                                   NamedElement.CALORIES_NUMBER.locator).text
        assert (number_calories == volume), f"Что то пошло не так с калориями, фактическое значение {number_calories}"
        print("Верное число калорий в форме")
