from selenium.webdriver.common.by import By
import enum


class NamedElement(enum.Enum):
    def __init__(self, search_method, locator, locator_name):
        self.search_method = search_method
        self.locator = locator
        self.locator_name = locator_name

    AGE = (By.NAME, "age", "Поле ввода 'Возраст'")
    HEIGHT = (By.NAME, "height", "Поле ввода 'Рост'")
    WEIGHT = (By.NAME, "weight", "Поле ввода 'Вес'")
    GENDER = (By.ID, "radio-2", "Поле выбора пола 'Мужской'")
    TYPE = (By.ID, "radio-3", "Цель Сбросить вес")
    VOLUME_ACTION = (By.XPATH, "//input[@name='obr' and @value='3']", "Поле выбора 'Активность Средняя'")
    SUBMIT = (By.ID, "button", "Кнопка Вычислить")
    CALORIES_NUMBER = (By.ID, "callor", "Необходимое количество калорий в день")
    NUMBER_BAZAL_META = (By.XPATH, "//table[@id='res']/tbody/tr[3]/td[2]/b", "Число базального метаболизма")
