from datetime import date
from selenium.common.exceptions import NoSuchElementException
import allure
from pages.main_page import MainPage
import time
from pages.data import TestData


class TestCbzu:
    @allure.title("Позитивный тест расчёта КБЖУ")
    def test_positive_value(self, browser):
        new_user = MainPage(browser)
        new_user.open()

        # input info about user
        new_user.fill_value_positive()

        # check field
        time.sleep(4)
        new_user.check_calories(TestData.CORRECT_VOLUME)


    @allure.title("Негативный тест расчёта КБЖУ")
    def test_negative_value(self, browser):
        new_user = MainPage(browser)
        new_user.open()

        # input info about user
        new_user.fill_value_positive()
        time.sleep(5)

        # check field
        new_user.check_calories(TestData.WRONG_VOLUME)
